package com.noroff.hvz.services.player;

import com.noroff.hvz.repositories.PlayerRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.lang.Character.isLowerCase;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PlayerServiceImplTest {

    @Mock
    private PlayerRepository playerRepository;

    @InjectMocks
    private PlayerServiceImpl playerService;

    @Test
    @DisplayName("Random bite code length matches.")
    void givenPositiveLength_whenCreateRandomBiteCode_thenLengthMatches() {
        int biteCodeLength = 10;
        String randomBiteCode = playerService.createRandomBiteCode(biteCodeLength);
        assertEquals(biteCodeLength, randomBiteCode.length(), "Bite code length differs.");
    }

    @Test
    @DisplayName("Random bite code length is negative.")
    void givenNegativeLength_whenCreateRandomBiteCode_thenThrowsException() {
        int biteCodeLength = -10;
        String expectedMessage = PlayerServiceImpl.INVALID_BITE_CODE_LENGTH_MESSAGE + biteCodeLength;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            playerService.createRandomBiteCode(biteCodeLength);
        });
        assertEquals(expectedMessage, exception.getMessage(), "Exception message is not what expected");
    }

    @Test
    @DisplayName("Random bite code length is zero.")
    void givenNZeroLength_whenCreateRandomBiteCode_thenThrowsException() {
        int biteCodeLength = 0;
        String expectedMessage = PlayerServiceImpl.INVALID_BITE_CODE_LENGTH_MESSAGE + biteCodeLength;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            playerService.createRandomBiteCode(biteCodeLength);
        });
        assertEquals(expectedMessage, exception.getMessage(), "Exception message is not what expected");
    }

    @Test
    @DisplayName("Random bite code length is too high.")
    void givenN1001_whenCreateRandomBiteCode_thenThrowsException() {
        int biteCodeLength = 1001;
        String expectedMessage = PlayerServiceImpl.INVALID_BITE_CODE_LENGTH_MESSAGE + biteCodeLength;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            playerService.createRandomBiteCode(biteCodeLength);
        });
        assertEquals(expectedMessage, exception.getMessage(), "Exception message is not what expected");
    }

    @Test
    @DisplayName("Random bite code has capital letters.")
    void givenNormalInput_whenCreateRandomBiteCode_thenThrowsException() {
        int biteCodeLength = 10;
        String randomBiteCode = playerService.createRandomBiteCode(biteCodeLength);
        boolean allLowerCase=true;
        int i=0;
        char[] charArrayBiteCode = randomBiteCode.toCharArray();
        while(allLowerCase && i<randomBiteCode.length()){
            if(!isLowerCase(charArrayBiteCode[i])){
                allLowerCase=false;
            }
            i++;
        }
        assertTrue(allLowerCase, "Bite code is not all lowercase.");
    }

}