package com.noroff.hvz.controllers;

import com.noroff.hvz.mappers.KillMapper;
import com.noroff.hvz.models.DTOs.killDTOs.KillDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.kill.KillService;
import com.noroff.hvz.services.player.PlayerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = KillController.class)
class KillControllerTest {

    private static final int GAME_ID = 1;
    private static final int KILL_ID = 10;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KillService killService;

    @MockBean
    private PlayerService playerService;

    @MockBean
    private KillMapper killMapper;

    @MockBean
    private GameService gameService;

    @MockBean
    private WebSocketService webSocketService;

    private Game game;
    private Kill kill;
    private KillDTO killDTO;

    @BeforeEach
    void setup(){
        game = new Game();
        game.setId(GAME_ID);

        kill = new Kill();
        kill.setId(KILL_ID);
        kill.setLocation("Budapest");

        killDTO = new KillDTO();
        killDTO.setId(kill.getId());
        killDTO.setGame(GAME_ID);
        killDTO.setLocation(kill.getLocation());
    }

    @Test
    @WithMockUser(value = "spring")
    @DisplayName("Find all kills by game id")
    void findAllByGameId() throws Exception {
        List<Kill> kills = List.of(kill);
        Mockito.when(killService.findAllByGameId(GAME_ID)).thenReturn(kills);
        Mockito.when(killMapper.killToKillDto(kills)).thenReturn(List.of(killDTO));
        Mockito.when(gameService.findById(GAME_ID)).thenReturn(game);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/game/" + GAME_ID + "/kill"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(10))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].location").value(kill.getLocation()));



    }
}