package com.noroff.hvz.controllers;

import com.noroff.hvz.mappers.PlayerMapper;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.loginUser.LoginUserService;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PlayerController.class)
class PlayerControllerTest {

    private static final int GAME_ID = 1;
    private static final int PLAYER_ID = 14;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;

    @MockBean
    private PlayerMapper playerMapper;

    @MockBean
    private LoginUserService loginUserService;

    @MockBean
    private GameService gameService;

    @MockBean
    private WebSocketService webSocketService;

    @MockBean
    private SquadService squadService;

    @MockBean
    private SquadMemberService squadMemberService;

    @MockBean
    private ChatService chatService;

    private Game game;
    private Player player;
    private PlayerDTO playerDTO;

    @BeforeEach
    void setup() {
        game = new Game();
        game.setId(GAME_ID);

        player = new Player();
        player.setId(PLAYER_ID);
        player.setChats(Collections.emptySet());
        player.setHuman(true);
        player.setPatientZero(false);
        player.setBiteCode("lajsdfoisdz");

        playerDTO = new PlayerDTO();
        playerDTO.setId(player.getId());
        playerDTO.setGame(GAME_ID);
        playerDTO.setHuman(player.isHuman());
        playerDTO.setPatientZero(player.isPatientZero());
        playerDTO.setBiteCode(player.getBiteCode());
    }

    @Test
    @WithMockUser(value = "spring")
    @DisplayName("Find player by id happy path.")
    void givenPlayerExists_whenFindByPlayerId_thenReturnPlayerDto() throws Exception {
        when(gameService.findById(GAME_ID)).thenReturn(game);
        when(playerService.findByGameIdAndPlayerId(GAME_ID, PLAYER_ID)).thenReturn(player);
        when(playerMapper.playerToPlayerDto(player)).thenReturn(playerDTO);
        mockMvc.perform(get("/api/game/"+ GAME_ID + "/player/" + PLAYER_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(PLAYER_ID))
                .andExpect(jsonPath("$.biteCode").value("lajsdfoisdz"))
                .andExpect(jsonPath("$.game").value(GAME_ID))
                .andExpect(jsonPath("$.isPatientZero").value(false))
                .andExpect(jsonPath("$.isHuman").value(true));
    }

}