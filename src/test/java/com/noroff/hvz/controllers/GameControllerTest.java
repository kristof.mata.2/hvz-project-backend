package com.noroff.hvz.controllers;

import com.noroff.hvz.mappers.GameMapper;
import com.noroff.hvz.models.DTOs.GameDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = GameController.class)
class GameControllerTest {

    private static final int GAME_ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private GameMapper gameMapper;

    @MockBean
    private PlayerService playerService;

    @MockBean
    private SquadService squadService;

    @MockBean
    private WebSocketService webSocketService;

    @MockBean
    private SquadMemberService squadMemberService;

    @MockBean
    private ChatService chatService;

    private Game game;
    private GameDTO gameDTO;


    @BeforeEach
    void setUp() {
        game = new Game();
        game.setId(GAME_ID);
        game.setName("Test Game");
        game.setState("Registration");

        gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setName("Test Game");
        gameDTO.setState("Registration");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @WithMockUser(value = "spring")
    @DisplayName("Find game by ID happy path.")
    void givenGameExists_whenFindByGameId_thenReturnGameDTO() throws Exception {
        Mockito.when(gameService.findById(GAME_ID)).thenReturn(game);
        Mockito.when(gameMapper.gameToGameDto(game)).thenReturn(gameDTO);

        mockMvc.perform(get("/api/game/" + GAME_ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(GAME_ID))
                .andExpect(jsonPath("$.name").value(gameDTO.getName()))
                .andExpect(jsonPath("$.state").value(gameDTO.getState()));
    }
}