package com.noroff.hvz.controllers;

import com.noroff.hvz.mappers.ChatMapper;
import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.DTOs.ChatDTO;

import com.noroff.hvz.repositories.ChatRepository;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.util.List;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = ChatController.class)
class ChatControllerTest {
    private static final int GAME_ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChatService chatService;

    @MockBean
    private GameService gameService;

    @MockBean
    private ChatMapper chatMapper;

    @MockBean
    private WebSocketService webSocketService;

    @MockBean
    private ChatRepository chatRepository;

    private Chat chat;

    private ChatDTO chatDTO;

    @BeforeEach
    void setup() {
        chat = new Chat();

        chat.setId(1);
        chat.setMessage("asf");


        chatDTO = new ChatDTO();
        chatDTO.setId(chat.getId());
        chatDTO.setMessage(chat.getMessage());

    }

    @Test
    @WithMockUser(value = "spring")
    @DisplayName("Find all chat messages by game id.")
    void findAllByGame() throws Exception {
        List<Chat> chats = List.of(chat);
        Mockito.when(chatService.findAllByGameId(GAME_ID)).thenReturn(List.of(chat));
        Mockito.when(chatMapper.chatToChatDto(chats)).thenReturn(List.of(chatDTO));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/game/" + GAME_ID + "/chat/all"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].message").value(chat.getMessage()));

    }
}