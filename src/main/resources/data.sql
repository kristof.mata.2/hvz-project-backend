--USERS
INSERT INTO tb_user (first_name,last_name, keycloak_id, user_name)
VALUES ('Mata','Kristof', '41bf7139-1563-4294-a714-eb00db036b83','matak');

INSERT INTO tb_user (first_name,last_name, keycloak_id, user_name)
VALUES ('KP','User', '039e600f-f81e-4abd-a0a0-f942d870892b','kpuser');

INSERT INTO tb_user (first_name,last_name, keycloak_id, user_name)
VALUES ('Test1','Test1', '693dbf60-461c-41c7-87bf-08a2f76ffdf7','test1');

INSERT INTO tb_user (first_name,last_name, keycloak_id, user_name)
VALUES ('Bela','toth', 'ff06f254-fc78-4498-ae3f-815435338484','	tothbt');

INSERT INTO tb_user (first_name,last_name, keycloak_id, user_name)
VALUES ('user','user', '76a116e9-7fc5-4fdf-bf7b-7ad59abdb600','user');
--GAMES
INSERT INTO tb_game (name, state, human_count, location)
VALUES ('Budapest HvZ','Registration',5,'Budapest');

INSERT INTO tb_game (name, state, human_count, location)
VALUES ('Eger HvZ','Registration',2,'Eger');

INSERT INTO tb_game (name, state, human_count, location)
VALUES ('New York HvZ','Registration',3,'New York');

INSERT INTO tb_game (name, state, human_count, location)
VALUES ('Berlin HvZ','Registration',0,'Berlin');

INSERT INTO tb_game (name, state, human_count, location)
VALUES ('Hamburg HvZ','Registration',0,'Hamburg');
--PLAYERS
--game 1
INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',1,1,'ygym');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',2,1,'asdf');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',3,1,'qwer');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',4,1,'yxcv');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',5,1,'ffff');
--game 2
INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',1,2,'yxcv');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',2,2,'ffff');
--game 3
INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',1,3,'yxcv');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',2,3,'ffff');

INSERT INTO tb_player (is_patient_zero, is_human, login_user_id, game_id, bite_code)
VALUES ('false','true',3,3,'qwer');


--INSERT INTO tb_squad (name,game_id)
--VALUES ('Cats',1);

--INSERT INTO tb_squad (name,game_id)
--VALUES ('Bunnies',2);

--INSERT INTO tb_squadmember (rank,player_id,squad_id)
--VALUES ('doctor',1,1);

--INTO tb_squadmember (rank,player_id,squad_id)
--VALUES ('soldier',2,1);

--INSERT INTO tb_player (is_patient_zero, is_human, login_user_id)
--VALUES ('false','false',1);

--I assume killer and victim needed
--INSERT INTO tb_kill (longitude, latitude,game)
--VALUES ('15','15',1);





