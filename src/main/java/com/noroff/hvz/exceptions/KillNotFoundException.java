package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class KillNotFoundException extends RuntimeException {

    public KillNotFoundException(Integer killId){
        super("Kill with id: " + killId + " does not exist");
    }

    public KillNotFoundException(Integer gameId, Integer searchedId){
        super("Searched id: " + searchedId + " in game: "+gameId+" does not exist.");
    }
}
