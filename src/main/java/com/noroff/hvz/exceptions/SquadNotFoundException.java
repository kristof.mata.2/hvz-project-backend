package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SquadNotFoundException extends RuntimeException {

    public SquadNotFoundException(int searchedId) {
        super("Squad does not exist with searched ID: "+ searchedId+".");
    }
    public SquadNotFoundException(int gameId, int squadId) {
        super("Game with ID: " + gameId + " does not have squad with ID:" + squadId);
    }

    public SquadNotFoundException(int gameId, String squadName) {
        super("Game with ID: " + gameId + " does not have squad with name:" + squadName);
    }
}
