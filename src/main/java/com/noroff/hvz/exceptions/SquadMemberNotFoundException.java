package com.noroff.hvz.exceptions;

public class SquadMemberNotFoundException extends RuntimeException {
    public SquadMemberNotFoundException(Integer gameId, Integer searchedId){
        super("Squad member with searched id: " + searchedId + " in game: "+gameId+" does not exist.");
    }

    public SquadMemberNotFoundException(Integer searchedId) {
        super("Squad member with searched id: " + searchedId + " does not exist.");
    }
}
