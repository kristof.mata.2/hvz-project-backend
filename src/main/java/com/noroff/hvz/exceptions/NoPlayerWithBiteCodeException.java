package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoPlayerWithBiteCodeException extends RuntimeException {
    public NoPlayerWithBiteCodeException(String biteCode) {
        super("LoginUser with keycloakId: " + biteCode + " does not exist");
    }
}
