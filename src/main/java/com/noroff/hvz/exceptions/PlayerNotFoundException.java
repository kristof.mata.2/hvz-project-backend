package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PlayerNotFoundException extends RuntimeException {

    public PlayerNotFoundException(Integer id){
        super("Player with id: " + id + " does not exist");
    }

    public PlayerNotFoundException(Integer gameId, Integer searchedId){
        super("Player with searched id: " + searchedId + " in game: "+gameId+" does not exist.");
    }

    public PlayerNotFoundException(Integer gameId, String searchedId){
        super("Player with searched id: " + searchedId + " in game: "+gameId+" does not exist.");
    }
}

