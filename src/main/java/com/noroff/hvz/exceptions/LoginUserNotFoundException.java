package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class LoginUserNotFoundException extends RuntimeException {

    public LoginUserNotFoundException(String searchedId){
        super("LoginUser with keycloakId: " + searchedId + " does not exist");
    }

    /*public LoginUserNotFoundException(Integer gameId, Integer searchedId){
        super("Searched id: " + searchedId + " in game: "+gameId+" does not exist.");
    }*/
}

