package com.noroff.hvz.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ChatNotFoundException extends RuntimeException{
    public ChatNotFoundException(Integer chatId){
        super("Chat with id:" + chatId + "does not exist!");
    }
}