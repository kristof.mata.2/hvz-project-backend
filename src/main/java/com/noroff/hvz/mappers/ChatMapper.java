package com.noroff.hvz.mappers;

import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.DTOs.ChatDTO;
import com.noroff.hvz.models.DTOs.killDTOs.KillDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.CreatePlayerDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.player.PlayerService;
import jdk.jfr.Name;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class ChatMapper {

    @Autowired
    protected PlayerService playerService;
    @Autowired
    protected GameService gameService;

    //Chat -> ChatDto
    @Mapping(target = "game", source = "game.id")
    @Mapping(target = "player", source = "player.id")
    public abstract ChatDTO chatToChatDto(Chat chat);
    public abstract Collection<ChatDTO> chatToChatDto(Collection<Chat> chats);

    //ChatDto -> Chat
    @Mapping(target = "game", source = "game", qualifiedByName = "gameIdToGame")
    @Mapping(target = "player", source = "player", qualifiedByName = "playerIdToPlayer")
    public abstract Chat chatDtoToChat(ChatDTO chatDTO);

    @Named("gameIdToGame")
    Game mapIdToGame(Integer id){
        if(id != null){
            return gameService.findById(id);
        }
        return null;
    }

    @Named("playerIdToPlayer")
    Player mapIdToPlayer(Integer id){
        if (id != null){
            return  playerService.findById(id);
        }
        return null;
    }


}
