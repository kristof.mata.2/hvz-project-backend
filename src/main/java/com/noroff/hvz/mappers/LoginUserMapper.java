package com.noroff.hvz.mappers;

import com.noroff.hvz.models.DTOs.LoginUserDTO;
import com.noroff.hvz.models.LoginUser;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.player.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;



@Mapper(componentModel = "spring")
public abstract class LoginUserMapper {

    @Autowired
    protected PlayerService playerService;

    //LoginUser -> LoginUserDTO
    @Mapping(target = "players", source = "players", qualifiedByName = "playersToIds")
    public abstract LoginUserDTO loginUserToLoginUserDto(LoginUser loginUser);
    public abstract Collection<LoginUserDTO> loginUserToLoginUserDto(Collection<LoginUser> loginUser);
    //Methods
    @Named("playersToIds")
    Set<Integer> mapPlayersToIds(Set<Player> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(player -> player.getId())
                .collect(Collectors.toSet());
    }

    //LoginUserDTO -> LoginUser
    @Mapping(target = "players", source = "players", qualifiedByName = "playersIdToPlayers")
    public abstract LoginUser loginUserDtoToLoginUser(LoginUserDTO loginUserDTO);
    //Methods
    @Named("playersIdToPlayers")
    Set<Player> mapIdToPlayer(Set<Integer> playerIds) {
        if (playerIds == null) {
            return new HashSet<>();
        }
        return playerIds.stream()
                .map( playerId -> playerService.findById(playerId))
                .collect(Collectors.toSet());
    }

}

