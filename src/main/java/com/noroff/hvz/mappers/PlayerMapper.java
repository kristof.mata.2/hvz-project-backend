package com.noroff.hvz.mappers;

import com.noroff.hvz.models.*;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerAdminDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.CreatePlayerDTO;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.kill.KillService;
import com.noroff.hvz.services.loginUser.LoginUserService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import jdk.jfr.Name;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class PlayerMapper {
    @Autowired
    protected LoginUserService loginUserService;

    @Autowired
    protected GameService gameService;

    @Autowired
    protected SquadMemberService squadMemberService;

    @Autowired
    protected KillService killService;

    @Autowired
    protected ChatService chatService;

    //Player -> PlayerDTO
    @Mapping(target = "loginUser", source = "loginUser.id")
    @Mapping(target = "game", source = "game.id")
    @Mapping(target = "squadMember", source = "squadMember.id")
    @Mapping(target = "chats",source = "chats", qualifiedByName = "chatsToIds")
    public abstract PlayerDTO playerToPlayerDto(Player player);
    public abstract Collection<PlayerDTO> playerToPlayerDto(Collection<Player> players);
    //ADMIN
    @Mapping(target = "loginUser", source = "loginUser.id")
    @Mapping(target = "game", source = "game.id")
    @Mapping(target = "squadMember", source = "squadMember.id")
    @Mapping(target = "chats",source = "chats", qualifiedByName = "chatsToIds")
    @Mapping(target = "username",source = "loginUser", qualifiedByName = "userIdToUsername" )
    public abstract PlayerAdminDTO playerToPlayerAdminDto(Player player);
    public abstract Collection<PlayerAdminDTO> playerToPlayerAdminDto(Collection<Player> player);
    //Methods
    @Named("userIdToUsername")
    String mapUserIdToUsername(LoginUser loginUser) {
        if(loginUser != null) {
            return loginUser.getUserName();
        }
        return null;
    }

    //PlayerDTO -> Player
    @Mapping(target = "loginUser", source = "loginUser", qualifiedByName = "loginUserIdToLoginUser")
    @Mapping(target = "game", source = "game", qualifiedByName = "gameIdToGame")
    @Mapping(target = "squadMember", source = "squadMember", qualifiedByName = "squadMemberIdToSquadMember")
    @Mapping(target = "chats", source = "chats", qualifiedByName = "chatIdToChats")
    public abstract Player playerDtoToPlayer(PlayerDTO playerDto);
    //Create Player
    @Mapping(target = "loginUser", source = "loginUser", qualifiedByName = "loginUserIdToLoginUser")
    @Mapping(target = "game", source = "game", qualifiedByName = "gameIdToGame")
    @Mapping(target = "squadMember", source = "squadMember", qualifiedByName = "squadMemberIdToSquadMember")
    @Mapping(target = "chats", source = "chats", qualifiedByName = "chatIdToChats")
    public abstract Player createPlayerDtoToPlayer(CreatePlayerDTO createPlayerDTO);
    //Methods
    @Named("loginUserIdToLoginUser")
    LoginUser mapIdToUser(Integer id) {
        if(id != null) {
            return loginUserService.findById(id);
        }
        return null;
    }
    @Named("gameIdToGame")
    Game mapIdToGame(Integer id) {
        if(id != null) {
            return gameService.findById(id);
        }
        return null;
    }
    @Named("squadMemberIdToSquadMember")
    SquadMember mapIdToSquadMember(Integer id) {
        if(id != null) {
            return squadMemberService.findById(id);
        }
        return null;
    }

    @Named("chatIdToChats")
    Set<Chat> mapChatIdsToPlayers(Set<Integer> chatIds){
        if(chatIds == null){
            return new HashSet<>();
        }
        return chatIds.stream()
                .map( chatid -> chatService.findById(chatid))
                .collect(Collectors.toSet());
    }

    @Named("chatsToIds")
    Set<Integer> mapChatToIds(Set<Chat> source){
        if (source == null){
            return null;
        }
        return source.stream()
                .map(c -> c.getId()).collect(Collectors.toSet());
    }
}
