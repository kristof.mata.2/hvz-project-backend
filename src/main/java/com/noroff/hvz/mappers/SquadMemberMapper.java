package com.noroff.hvz.mappers;

import com.noroff.hvz.models.DTOs.squadMemberDTOs.SquadMemberDTO;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class SquadMemberMapper {

    @Autowired
    protected SquadService squadService;

    @Autowired
    protected PlayerService playerService;
    //SquadMember -> SquadMemberDTO
    @Mapping(target = "squad", source = "squad.id")
    @Mapping(target = "player", source = "player.id")
    public abstract SquadMemberDTO squadMemberToSquadMemberDto(SquadMember squadMember);
    public abstract Collection<SquadMemberDTO> squadMemberToSquadMemberDto(Collection<SquadMember> squadMembers);
    //Methods

    //SquadMemberDTO -> SquadMember
    @Mapping(target = "squad", source = "squad", qualifiedByName = "squadIdToProject")
    @Mapping(target = "player", source = "player", qualifiedByName = "playerIdToProject")
    public abstract SquadMember squadMemberDtoToSquadMember(SquadMemberDTO squadMemberDTO);
    //Methods
    @Named("squadIdToProject")
    Squad mapIdToSquad(Integer id) {
        if(id != null) {
            return squadService.findById(id);
        }
        return null;
    }
    @Named("playerIdToProject")
    Player mapIdToPlayer(Integer id) {
        if(id != null) {
            return playerService.findById(id);
        }
        return null;
    }
}

