package com.noroff.hvz.mappers;

import com.noroff.hvz.models.DTOs.squadDTOs.SquadDTO;
import com.noroff.hvz.models.DTOs.squadDTOs.CreateSquadDTO;
import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.services.squad.SquadService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class SquadMapper {
    @Autowired
    protected SquadService squadService;
    @Autowired
    protected SquadMemberService squadMemberService;

    //SquadDTO -> Squad
    @Mapping(target = "squadMembers", source = "squadMembers", qualifiedByName = "squadMembersToIds")
    @Mapping(target = "game", source = "game.id")
    public abstract SquadDTO squadToSquadDto(Squad squad);
    public abstract Collection<SquadDTO> squadToSquadDto(Collection<Squad> squad);

    //CREATE SQUAD
    /*@Mapping(target = "squadMembers", source = "squadMembers", qualifiedByName = "squadMembersToIds")
    @Mapping(target = "game", source = "game.id")
    public abstract CreateSquadDTO squadToCreateSquadDto(Squad squad);
    public abstract Collection<CreateSquadDTO> squadToCreateSquadDto(Collection<Squad> squad);*/
    //Methods
    @Named("squadMembersToIds")
    Set<Integer> mapSquadMembersToIds(Set<SquadMember> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(squadMember -> squadMember.getId())
                .collect(Collectors.toSet());
    }

    //Squad -> SquadDTO
    @Mapping(target = "squadMembers", source = "squadMembers", qualifiedByName = "squadMemberIdToSquadMember")
    @Mapping(target = "game.id", source = "game")
    public abstract Squad squadDtoToSquad(SquadDTO squadDTO);
    //CREATE SQUAD
    @Mapping(target = "squadMembers", source = "squadMembers", qualifiedByName = "squadMemberIdToSquadMember")
    @Mapping(target = "game.id", source = "game")
    public abstract Squad createSquadDtoToSquad(CreateSquadDTO createSquadDTO);
    //Methods
    @Named("squadMemberIdToSquadMember")
    SquadMember mapIdToSquadMember(int id) {
        return squadMemberService.findById(id);
    }


}

