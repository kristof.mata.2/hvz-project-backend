package com.noroff.hvz.mappers;

import com.noroff.hvz.models.*;
import com.noroff.hvz.models.DTOs.GameDTO;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.kill.KillService;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import jdk.jfr.Name;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class GameMapper {
    @Autowired
    protected KillService killService;
    @Autowired
    protected PlayerService playerService;
    @Autowired
    protected SquadService squadService;

    @Autowired
    protected ChatService chatService;

    //Game -> GameDTO
    @Mapping(target = "squads", source = "squads", qualifiedByName = "squadsToIds")
    @Mapping(target= "kills", source = "kills", qualifiedByName = "killsToIds")
    @Mapping(target = "players", source = "players", qualifiedByName = "playersToIds")
    @Mapping(target = "chats", source = "chats", qualifiedByName = "chatsToIds")
    public abstract GameDTO gameToGameDto(Game game);
    public abstract Collection<GameDTO> gameToGameDto(Collection<Game> games);
    //Methods
    @Named("squadsToIds")
    Set<Integer> mapSquadsToIds(Set<Squad> source){
        if(source == null){
            return null;
        }
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());

    }
    @Named("killsToIds")
    Set<Integer> mapKillsToIds(Set<Kill> source){
        if(source == null){
            return null;
        }
        return source.stream()
                .map(k -> k.getId()).collect(Collectors.toSet());

    }
    @Named("playersToIds")
    Set<Integer> mapPlayersToIds(Set<Player> source){
        if(source == null){
            return null;
        }
        return source.stream()
                .map(p -> p.getId()).collect(Collectors.toSet());

    }
    @Named("chatsToIds")
    Set<Integer> mapChatToIds(Set<Chat> source){
        if (source == null){
            return null;
        }
        return source.stream()
                .map(c -> c.getId()).collect(Collectors.toSet());
    }

    //GameDTO -> Game
    @Mapping(target = "squads", source = "squads", qualifiedByName = "squadIdToSquads")
    @Mapping(target= "kills", source = "kills", qualifiedByName = "killIdToKills")
    @Mapping(target = "players", source = "players", qualifiedByName = "playerIdToPlayers")
    @Mapping(target = "chats", source = "chats", qualifiedByName = "chatIdToChats")
    public abstract Game gameDtoToGame(GameDTO dto);
    //Methods
    @Named("squadIdToSquads")
    Set<Squad> mapIdsToSquads(Set<Integer> squadIds){
        if (squadIds == null) {
            return new HashSet<>();
        }
        return squadIds.stream()
                .map( squadId -> squadService.findById(squadId))
                .collect(Collectors.toSet());
    }
    @Named("killIdToKills")
    Set<Kill> mapIdsToKills(Set<Integer> killIds){
        if (killIds == null) {
            return new HashSet<>();
        }
        return killIds.stream()
                .map( killId -> killService.findById(killId))
                .collect(Collectors.toSet());
    }
    @Named("playerIdToPlayers")
    Set<Player> mapIdsToPlayers(Set<Integer> PlayerIds){
        if (PlayerIds == null) {
            return new HashSet<>();
        }
        return PlayerIds.stream()
                .map( playerId -> playerService.findById(playerId))
                .collect(Collectors.toSet());
    }

    @Named("chatIdToChats")
    Set<Chat> mapChatIdsToPlayers(Set<Integer> chatIds){
        if(chatIds == null){
            return new HashSet<>();
        }
        return chatIds.stream()
                .map( chatid -> chatService.findById(chatid))
                .collect(Collectors.toSet());
    }


}
