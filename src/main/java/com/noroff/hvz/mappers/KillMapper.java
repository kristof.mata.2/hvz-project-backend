package com.noroff.hvz.mappers;

import com.noroff.hvz.models.DTOs.killDTOs.KillDTO;
import com.noroff.hvz.models.DTOs.killDTOs.CreateKillDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.player.PlayerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class KillMapper {

    @Autowired
    protected PlayerService playerService;
    @Autowired
    protected GameService gameService;

    //Kill -> KillDTO
    @Mapping(target = "game", source = "game.id")
    public abstract KillDTO killToKillDto(Kill kill);

    //Collection<Kill> -> Collection<KillDTO>
    public abstract Collection<KillDTO> killToKillDto(Collection<Kill> kills);

    //KillDTO -> Kill
    @Mapping(target = "game", source = "game", qualifiedByName = "gameIdToGame")
    public abstract Kill killDTOToKill(KillDTO  killDTO);

    //Kill -> CreateKillDTO
    @Mapping(target = "game", source = "game.id")
    public abstract CreateKillDTO killToCreateKillDto(Kill kill);

    //CreateKillDTO -> Kill
    @Mapping(target = "game", source = "game", qualifiedByName = "gameIdToGame")
    public abstract Kill createKillDtoToKill(CreateKillDTO createKillDTO);
    //Methods
    @Named("gameIdToGame")
    Game mapIdToGame(Integer gameId) {
        if(gameId != null) {
            return gameService.findById(gameId);
        }
        return null;
    }


}
