package com.noroff.hvz.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "tb_game")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 50, nullable = false)
    private String name;
    @Column(length = 20, nullable = false)
    private String state;
    @Column
    private String location;
    @Column
    private Date date;
    @OneToMany(mappedBy = "game")
    private Set<Squad> squads;
    @OneToMany(mappedBy = "game")
    private Set<Kill> kills;
    @OneToMany(mappedBy = "game")
    private Set<Player> players;
    @Column
    private Integer humanCount;
    @OneToMany(mappedBy = "game")
    private Set<Chat> chats;


    // Getters
    public String getLocation() {
        return location;
    }

    // Setters
    public void setLocation(String location) {
        this.location = location;
    }

}
