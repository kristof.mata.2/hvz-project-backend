package com.noroff.hvz.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "tb_kill")
public class Kill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private Date time;
    @Column
    private String location;
    @Column
    private Integer victimId;
    @Column
    private Integer killerId;
    @Column
    private String story;
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

}

