package com.noroff.hvz.models.DTOs.playerDTOs;

import com.noroff.hvz.models.Kill;
import com.noroff.hvz.models.SquadMember;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@Data
public class PlayerDTO {
    private Integer id;
    @JsonProperty("isPatientZero")
    private boolean isPatientZero;
    @JsonProperty("isHuman")
    private boolean isHuman;
    private String biteCode;
    private Integer loginUser;
    private Integer squadMember;
    private Integer game;
    private Set<Integer> chats;
}
