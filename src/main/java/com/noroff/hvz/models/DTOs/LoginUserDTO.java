package com.noroff.hvz.models.DTOs;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
public class LoginUserDTO {
    private Integer id;
    @NotBlank(message = "KeycloakId must not be null.")
    private String keycloakId;
    private String userName;
    private String firstName;
    private String lastName;
    private Set<Integer> players;
}
