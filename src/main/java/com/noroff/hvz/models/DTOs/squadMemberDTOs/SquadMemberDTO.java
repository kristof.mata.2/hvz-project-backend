package com.noroff.hvz.models.DTOs.squadMemberDTOs;

import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.models.Squad;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
public class SquadMemberDTO {
    private Integer id;
    private String rank;
    private Integer player;
    private Integer squad;
}
