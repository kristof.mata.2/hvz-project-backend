package com.noroff.hvz.models.DTOs.squadDTOs;

import lombok.Data;

import java.util.Set;

@Data
public class CreateSquadDTO {
        private Integer id;
        private Integer playerId;
        private String name;
        private Set<Integer> squadMembers;
        private Integer game;
}
