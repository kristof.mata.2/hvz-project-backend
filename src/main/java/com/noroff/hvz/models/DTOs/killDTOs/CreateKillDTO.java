package com.noroff.hvz.models.DTOs.killDTOs;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;

@Data
public class CreateKillDTO {
    private Integer id;
    private Integer victimId;
    private Integer killerId;
    @Size(max = 100, min = 1, message = "The length of the story has to be in range of 1 to 100")
    @Pattern(regexp = "^[^;\\/%]*$", message = "Story should not contain semicolon \"\n" +
            "            + \"and percentage character.\"" )
    private String story;
    private String location;
    @NotNull
    @NotBlank
    @Pattern(regexp = "[a-z]+", message = "Given biteCode is invalid!")
    private String biteCode;
    @PastOrPresent
    private Date time;
    @NotNull
    private Integer game;
}