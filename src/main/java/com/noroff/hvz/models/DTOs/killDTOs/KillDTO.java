package com.noroff.hvz.models.DTOs.killDTOs;

import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import lombok.Data;
import java.util.Date;
import java.util.Set;

@Data
public class KillDTO {

    private Integer id;
    private Date time;
    private String location;
    private Integer victimId;
    private Integer killerId;
    private Integer game;

}
