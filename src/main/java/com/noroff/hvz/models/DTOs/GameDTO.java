package com.noroff.hvz.models.DTOs;

import com.noroff.hvz.validator.CreateGameConstraint;
import com.noroff.hvz.validator.GameStatus;
import com.noroff.hvz.validator.UpdateGameConstraint;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.Set;

@Data
public class GameDTO {
    @Null(groups = CreateGameConstraint.class)
    @NotNull(groups = UpdateGameConstraint.class)
    private Integer id;
    @NotBlank
    private String name;
    @GameStatus
    private String state;
    private String location;
    private Date date;
    private Set<Integer> squads;
    private Set<Integer> kills;
    private Set<Integer> players;
    private Integer humanCount;
    private Set<Integer> chats;
}
