package com.noroff.hvz.models.DTOs;

import lombok.Data;

@Data
public class ChatDTO {
    private Integer id;
    private String message;
    private String faction;
    private String userName;
    private Integer player;
    private Integer game;
}