package com.noroff.hvz.models.DTOs.squadDTOs;


import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.SquadMember;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class SquadDTO {
    private int id;
    @NotBlank(message = "Name must not be empty.")
    private String name;
    private Set<Integer> squadMembers;
    @NotNull(message = "Game must not be null.")
    private Integer game;
}


