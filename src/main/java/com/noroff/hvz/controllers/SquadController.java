package com.noroff.hvz.controllers;

import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.MalformedRequestException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.mappers.PlayerMapper;
import com.noroff.hvz.mappers.SquadMapper;
import com.noroff.hvz.mappers.SquadMemberMapper;
import com.noroff.hvz.models.DTOs.squadDTOs.SquadDTO;
import com.noroff.hvz.models.DTOs.squadDTOs.CreateSquadDTO;
import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import com.noroff.hvz.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
 @CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class SquadController {

    private final SquadService squadService;
    private final SquadMapper squadMapper;
    private final SquadMemberService squadMemberService;
    private final SquadMemberMapper squadMemberMapper;
    private final PlayerService playerService;
    private final PlayerMapper playerMapper;
    private final GameService gameService;


    public SquadController(SquadService squadService, SquadMemberService squadMemberService, SquadMapper squadMapper, SquadMemberMapper squadMemberMapper, PlayerService playerService, PlayerMapper playerMapper, GameService gameService) {
        this.squadService = squadService;
        this.squadMemberService = squadMemberService;
        this.squadMapper = squadMapper;
        this.squadMemberMapper = squadMemberMapper;
        this.playerService = playerService;
        this.playerMapper = playerMapper;
        this.gameService = gameService;
    }

    @Operation(summary = "Retrieves all the squads in a game.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success.",
                    content = {@Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = SquadDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//GET AllByGameId
    @GetMapping("/{gameId}/squad")
    public ResponseEntity<Collection<SquadDTO>> findByGameId(@PathVariable("gameId") Integer gameId) {
        if (gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        Collection<SquadDTO> squads = squadMapper.squadToSquadDto(squadService.findAllByGameId(gameId));
        return ResponseEntity.ok(squads);
    }

    @Operation(summary = "Retrieves a specific squad by ID in a game.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = SquadDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Player does not exist with supplied gameId and killId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+kill",
                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//GET SquadByGameIdAndSquadId
    @GetMapping("/{gameId}/squad/{squadId}")
    public ResponseEntity getByGameIdAndSquadId(@PathVariable("gameId") Integer gameId, @PathVariable("squadId") Integer squadId) {
        if(gameService.findById(gameId)==null){
            throw new GameNotFoundException(gameId);
        }
        SquadDTO squad = squadMapper.squadToSquadDto(squadService.findBySquadIdAndGameId(squadId, gameId));
        return ResponseEntity.ok(squad);
    }

    @Operation(summary = "Adds a new Squad.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = SquadDTO.class))) }),
            @ApiResponse(responseCode = "500",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })//POST Add
    @PostMapping(path = "/{gameId}/squad")
    public ResponseEntity add(@PathVariable("gameId") Integer gameId, @RequestBody CreateSquadDTO createSquadDTO) {
        //TODO make this pretty
        /*
        //Getting the new Squad object from the DTO with the updated IDs, then updating the repo
        newSquad = squadMapper.squadDtoToSquad(squadDTO);
        squadService.update(newSquad);*/
        if(createSquadDTO.getPlayerId()==null){
            throw new MalformedRequestException();
        }
        System.out.println("PLAYER ID: "+createSquadDTO.getPlayerId());
        Squad squad = squadService.add(squadMapper.createSquadDtoToSquad(createSquadDTO));
        //Get the ID from repo using gameId and squadName.
        int newSquadId = squadService.findByNameInGame(gameId,createSquadDTO.getName()).getId();
        //Creating "squad leader" with "Leader" rank in the previously created squad.
        //And adding it to the repo for a squadMember ID
        SquadMember newSquadMember = new SquadMember();
        newSquadMember.setRank("Leader");
        newSquadMember.setSquad(squadService.findBySquadIdAndGameId(newSquadId,gameId));
        newSquadMember.setPlayer(playerService.findById(createSquadDTO.getPlayerId()));
        squadMemberService.add(newSquadMember);
        URI location = URI.create(gameId+"/squad/" + squad.getId());
        return ResponseEntity.created(location).build();
    }


    //TODO i have absolutely no clue what this is and why i made this lol
    // This needs to be in squadMemberController and needs a playerId

   /* @Operation(summary = "Adds a new SquadMember")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = SquadDTO.class))) }),
            @ApiResponse(responseCode = "500",
                    description = "Malformed request",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping(path = "/{gameId}/squad/{squadId}/join",
            produces = "application/json")
    public ResponseEntity add(@RequestBody SquadMemberDTO squadMemberDTO, @PathVariable("gameId") int gameId, @PathVariable("squadId") int squadId) {


        //Getting the new Squad object from the DTO with the updated IDs, then updating the repo
        SquadMember newSquadMember = squadMemberMapper.squadMemberDtoToSquadMember(squadMemberDTO);
        squadMemberService.add(newSquadMember);
        //squadService.update(newSquad);

        URI location = URI.create("api/squadmembers/"+newSquadMember.getId());
        return ResponseEntity.created(location).build();
    }*/

    /*
    @Operation(summary = "Player joins Squad")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Squad successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping(path = "/{gameId}/squad/{squadId}",
            produces = "application/json")
    public ResponseEntity update(@RequestBody SquadDTO squadDTO, @PathVariable("gameId") int gameId, @PathVariable("squadId") int squadId) {

        if(gameId != squadDTO.getGame() || squadId != squadDTO.getId())
            return ResponseEntity.badRequest().build();
        squadService.update(
                squadMapper.squadDtoToSquad(squadDTO)
        );
        return ResponseEntity.noContent().build();
    }
    */

    @Operation(summary = "Delete a Squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = SquadDTO.class))) }),
            @ApiResponse(responseCode = "405",
                    description = "Method Not Allowed",
                    content = { @Content( mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping(path = "/{gameId}/squad/{squadId}",
            produces = "application/json")
    public ResponseEntity delete(@PathVariable("gameId") int gameId, @PathVariable("squadId") int squadId) {
        System.out.println(squadId);
        Collection<SquadMember> squadMembers = squadMemberService.findAll();
        for(SquadMember m: squadMembers){
            if(m.getSquad().getId()==squadId){
                System.out.println("deleted"+m.getId());
                squadMemberService.deleteById(m.getId());
            }

        }
        squadService.deleteById(squadId);
        return ResponseEntity.noContent().build();
    }










}
