package com.noroff.hvz.controllers;

import com.noroff.hvz.mappers.LoginUserMapper;
import com.noroff.hvz.mappers.PlayerMapper;
import com.noroff.hvz.models.DTOs.LoginUserDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerDTO;
import com.noroff.hvz.models.LoginUser;
import com.noroff.hvz.services.loginUser.LoginUserService;
import com.noroff.hvz.services.player.PlayerService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


@RestController
@CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class LoginUserController {

    private final LoginUserService loginUserService;
    private final LoginUserMapper loginUserMapper;
    private final PlayerService playerService;
    private final PlayerMapper playerMapper;

    public LoginUserController(LoginUserService loginUserService, LoginUserMapper loginUserMapper, PlayerService playerService, PlayerMapper playerMapper) {
        this.loginUserService = loginUserService;
        this.loginUserMapper = loginUserMapper;
        this.playerService = playerService;
        this.playerMapper = playerMapper;
    }

    @Operation(summary = "Get all Users.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success.",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = LoginUserDTO.class))) })
    })//GET All
    @GetMapping("/loginuser")
    public ResponseEntity getAll(){
        Collection<LoginUserDTO> loginUserDTOs = loginUserMapper.loginUserToLoginUserDto(
                loginUserService.findAll()
        );
        return ResponseEntity.ok(loginUserDTOs);
    }
    @Operation(summary= "Get all the players of keycloakId")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = PlayerDTO.class))) })
    })//GET AllPlayerByKeycloakId
    @GetMapping("/loginuser/{keycloakId}/player") //http://localhost:8080/api/game/loginuser/{keycloakId}/player
    public ResponseEntity getPlayersByKeycloakId(@PathVariable("keycloakId") String keycloakId){
        if(loginUserService.findByKeycloakId(keycloakId)==null){
            return ResponseEntity.notFound().build();
        }
        Collection<PlayerDTO> players = playerMapper.playerToPlayerDto(
                playerService.findByKeycloakId(keycloakId)
        );
        return ResponseEntity.ok(players);
    }

    @Operation(summary = "Adds new LoginUser.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="LoginUser successfully created.",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = LoginUserDTO.class)) }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)
    })//POST Add
    @PostMapping("/loginuser")
    public ResponseEntity add(@RequestBody LoginUserDTO loginUserDTO) {
        LoginUser loginUser = loginUserService.add(loginUserMapper.loginUserDtoToLoginUser(loginUserDTO));
        URI location = URI.create("/loginuser" + loginUser.getId());
        return ResponseEntity.created(location).build();
    }

}
