package com.noroff.hvz.controllers;

import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.KillNotFoundException;
import com.noroff.hvz.exceptions.MalformedRequestException;
import com.noroff.hvz.exceptions.NoPlayerWithBiteCodeException;
import com.noroff.hvz.mappers.KillMapper;
import com.noroff.hvz.models.DTOs.killDTOs.KillDTO;
import com.noroff.hvz.models.DTOs.killDTOs.CreateKillDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.kill.KillService;
import com.noroff.hvz.services.player.PlayerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.Date;

@Validated
@RestController
@CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class KillController {

    private final KillService killService;
    private final PlayerService playerService;
    private final KillMapper killMapper;
    private final GameService gameService;
    private final WebSocketService webSocketService;

    public KillController(KillService killService, PlayerService playerService, KillMapper killMapper, GameService gameService, WebSocketService webSocketService) {
        this.killService = killService;
        this.playerService = playerService;
        this.killMapper = killMapper;
        this.gameService = gameService;
        this.webSocketService = webSocketService;
    }


    @Operation(summary = "Retrieves all the kills in a game.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = KillDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//GET AllByGameId
    @GetMapping("/{gameId}/kill")
    public ResponseEntity findAllByGameId(@PathVariable("gameId") Integer gameId) {
        if (gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        Collection<KillDTO> killDTOs = killMapper.killToKillDto(killService.findAllByGameId(gameId));
        return ResponseEntity.ok(killDTOs);
    }

    @Operation(summary= "Retrieves a specific kill by ID in a game.")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = KillDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Kill does not exist with supplied gameId and killId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                                @Content(mediaType ="application/json+kill",
                    schema = @Schema(implementation = KillNotFoundException.class))})
    })//GET KillByGameIdAndKillId
    @GetMapping("/{gameId}/kill/{killId}")
    public ResponseEntity findByGameIdAndKillId(@PathVariable("gameId") int gameId, @PathVariable("killId") int killId) {
        if(gameService.findById(gameId)==null){
            throw new GameNotFoundException(gameId);
        }
        KillDTO killDTO = killMapper.killToKillDto(killService.findByGameIdAndKillId(gameId, killId));
        return ResponseEntity.ok(killDTO);
    }


    @Operation(summary = "Adds a new Kill.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="Kill successfully created.",
                    content = @Content ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request.",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = NoPlayerWithBiteCodeException.class)),
                                @Content(mediaType = "application/json+already+dead",
                    schema = @Schema(implementation = MalformedRequestException.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//POST Add
    @PostMapping("/{gameId}/kill")
    public ResponseEntity add(@PathVariable("gameId") Integer gameId,@Valid @RequestBody CreateKillDTO createKillDTO) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }

        Game game = gameService.findById(gameId);
        if(createKillDTO.getKillerId()==null){
            createKillDTO.setKillerId(-1);//If admin killed killer id is -1
        }
        Player victim = playerService.findByGameIdBiteCode(gameId, createKillDTO.getBiteCode());
        if(victim.isHuman()==false){
            throw new MalformedRequestException();
        }
        // Set player to victim
        createKillDTO.setVictimId(victim.getId());
        // Set player to !Human
        victim.setHuman(false);
        // set time of death
        createKillDTO.setTime(new Date());
        // Set location of kill
        createKillDTO.setLocation(gameService.findById(gameId).getLocation());
        game.setHumanCount(game.getHumanCount()-1);
        gameService.update(game);
        Kill newKill = killService.add(killMapper.createKillDtoToKill(createKillDTO));
        URI location = URI.create(gameId + "/kill/" + newKill.getId());
        checkGameOver(gameId);//Game over check
        //Websocket
            //details
            webSocketService.sendMessage("kill/"+victim.getId(),"player_died");
            //admin
            webSocketService.sendMessage("kill/"+gameId,"player_died");
        return ResponseEntity.created(location).build();
    }

    //Game Over check
    private void checkGameOver(Integer gameId) {
        Game game = gameService.findById(gameId);
        if(game.getState().equals("In Progress")){
            if(game.getHumanCount()==1){
                game.setState("Complete");
                gameService.update(game);
            }
        }
    }

    @Operation(summary = "Updates a Kill object")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated the Kill",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MalformedRequestException.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Kill does not exist with supplied gameId and killId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+kill",
                    schema = @Schema(implementation = KillNotFoundException.class))})
    })//PUT update
    @PutMapping("/{gameId}/kill/{killId}")
    public ResponseEntity<KillDTO> update(@RequestBody KillDTO killDTO, @PathVariable("gameId") int gameId, @PathVariable("killId") int killId) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        if(killService.findByGameIdAndKillId(gameId,killId)==null){
            throw new KillNotFoundException(killId);
        }
        killDTO.setId(killId);
        killService.update(
                killMapper.killDTOToKill(killDTO)
        );
        //Websocket
            //webSocketService.sendMessage("kill/"+killId,"update");
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a specific kill by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "204",
                    description = "Kill deleted.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = KillDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Kill does not exist with supplied gameId and killId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+kill",
                    schema = @Schema(implementation = KillNotFoundException.class))})
    })//DELETE delete
    @DeleteMapping("/{gameId}/kill/{killId}")
    public ResponseEntity delete(@PathVariable("gameId") int gameId, @PathVariable("killId") int killId) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        if(killService.findByGameIdAndKillId(gameId,killId)==null){
            throw new KillNotFoundException(killId);
        }
        killService.deleteById(killId);
        //Websocket
        //webSocketService.sendMessage("kill/"+killId,"delete");
        return ResponseEntity.noContent().build();
    }

}
