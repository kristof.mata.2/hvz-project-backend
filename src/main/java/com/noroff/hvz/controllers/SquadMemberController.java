package com.noroff.hvz.controllers;

import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.MalformedRequestException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.exceptions.SquadMemberNotFoundException;
import com.noroff.hvz.mappers.SquadMemberMapper;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerDTO;
import com.noroff.hvz.models.DTOs.squadMemberDTOs.SquadMemberDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
 @CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class SquadMemberController {

    private final SquadMemberService squadMemberService;
    private final SquadMemberMapper squadMemberMapper;
    private final GameService gameService;

    public SquadMemberController(SquadMemberService squadMemberService, SquadMemberMapper squadMemberMapper, GameService gameService) {
        this.squadMemberService = squadMemberService;
        this.squadMemberMapper = squadMemberMapper;
        this.gameService = gameService;
    }

    @Operation(summary = "Get all squads members in a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success.",
                    content = {@Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = SquadMemberDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })
    @GetMapping("/{gameId}/squadmember")
    public ResponseEntity findByGameId(@PathVariable("gameId") Integer gameId) {
        Collection<SquadMemberDTO> squadsMembers = squadMemberMapper.squadMemberToSquadMemberDto(
                squadMemberService.findAllByGameId(gameId)
        );
        return ResponseEntity.ok(squadsMembers);
    }

    @Operation(summary= "Retrieves a specific squad member by ID in a game.")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = PlayerDTO.class))

                    }),
            @ApiResponse(responseCode = "404",
                    description = "Squad member does not exist with supplied gameId and squadMemberId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                                @Content(mediaType ="application/json+squad+member",
                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//GET SquadMemberByGameIdAndSquadMemberId
    @GetMapping("/{gameId}/squadmember/{squadMemberId}")
    public ResponseEntity<SquadMemberDTO> findBySquadMemberId(@PathVariable("gameId") int gameId, @PathVariable("squadMemberId") int squadMemberId) {
        if(gameService.findById(gameId)==null){
            throw new GameNotFoundException(gameId);
        }
        SquadMemberDTO squadMemberDTO = squadMemberMapper.squadMemberToSquadMemberDto(squadMemberService.findByGameIdAndSquadMemberId(gameId, squadMemberId));
        return ResponseEntity.ok(squadMemberDTO);
    }

    @Operation(summary = "Adds a new SquadMember.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="The SquadMember is successfully added",
                    content = @Content ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request.",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MalformedRequestException.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//POST Add
    @PostMapping("{gameId}/squadmember")
    public ResponseEntity add(@PathVariable("gameId") Integer gameId, @RequestBody SquadMemberDTO squadMemberDTO) {
        Game game = gameService.findById(gameId);
        if(game==null) {
            throw new GameNotFoundException(gameId);
        }
        if(squadMemberDTO.getSquad()==null || squadMemberDTO.getPlayer()==null){
            throw new MalformedRequestException();
        }
        SquadMember squadMember = squadMemberService.add(squadMemberMapper.squadMemberDtoToSquadMember(squadMemberDTO));
        URI location = URI.create(gameId+"/player/" + squadMember.getId());
        //Websocket
            //login
            //webSocketService.sendMessage("game","create_players");
            //details
            //webSocketService.sendMessage("player/"+player.getId(),"create");
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Updates a Squad member")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated the Squad member",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Squad member does not exist with supplied gameId and squadMemberId.",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+player",
                                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//PUT update
    @PutMapping("/{gameId}/squadmember/{squadMemberId}")
    public ResponseEntity<SquadMemberDTO> update(@RequestBody SquadMemberDTO squadMemberDTO, @PathVariable("gameId") int gameId, @PathVariable("squadMemberId") int squadMemberId) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        if(squadMemberService.findByGameIdAndSquadMemberId(gameId,squadMemberId)==null){
            throw new SquadMemberNotFoundException(gameId,squadMemberId);
        }
        squadMemberDTO.setId(squadMemberId);
        SquadMember oldSquadMember = squadMemberService.findByGameIdAndSquadMemberId(gameId,squadMemberDTO.getId());
        if(squadMemberDTO.getSquad()==null){
            squadMemberDTO.setSquad(oldSquadMember.getSquad().getId());
        }
        if(squadMemberDTO.getPlayer()==null){
            squadMemberDTO.setPlayer(oldSquadMember.getPlayer().getId());
        }
        squadMemberService.update(
                squadMemberMapper.squadMemberDtoToSquadMember(squadMemberDTO)
        );
        //Websocket
            //login
            //details
            //webSocketService.sendMessage("player/"+playerId,"update");
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a specific squad member by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "204",
                    description = "Squad member deleted.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = PlayerDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Squad member does not exist with supplied gameId and playerId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                                @Content(mediaType ="application/json+player",
                   schema = @Schema(implementation = SquadMemberNotFoundException.class))})
    })//DELETE delete
    @DeleteMapping(path = "/{gameId}/squadmember/{squadMemberId}")
    public ResponseEntity delete(@PathVariable("gameId") int gameId, @PathVariable("squadMemberId") int squadMemberId) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        if(squadMemberService.findByGameIdAndSquadMemberId(gameId,squadMemberId)==null){
            throw new SquadMemberNotFoundException(gameId, squadMemberId);
        }
        squadMemberService.deleteById(squadMemberId);
        //Websocket
            //login
            //webSocketService.sendMessage("game","delete_players");
            //details
            //webSocketService.sendMessage("player/"+playerId,"delete");
        return ResponseEntity.noContent().build();
    }


}
