package com.noroff.hvz.controllers;

import com.noroff.hvz.exceptions.ChatNotFoundException;
import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.MalformedRequestException;
import com.noroff.hvz.mappers.ChatMapper;
import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.DTOs.ChatDTO;
import com.noroff.hvz.repositories.ChatRepository;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class ChatController {
    private final ChatRepository chatRepository;
    private final ChatMapper chatMapper;
    private final ChatService chatService;

    private final GameService gameService;
    private final WebSocketService webSocketService;

    public ChatController(ChatRepository chatRepository, ChatMapper chatMapper, ChatService chatService, GameService gameService, WebSocketService webSocketService) {
        this.chatRepository = chatRepository;
        this.chatMapper = chatMapper;
        this.chatService = chatService;
        this.gameService = gameService;
        this.webSocketService = webSocketService;
    }

    @Operation(summary = "Get all Messages")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ChatDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ChatNotFoundException.class)) })
    })
    @GetMapping(path = "/{gameId}/chat") //api/game/1/chat?faction=global
    public ResponseEntity findAllByGameIdAndFaction(@PathVariable("gameId") Integer gameId, @RequestParam String faction) {
        /**
        if(faction.equals("global")){
            Collection<ChatDTO> chatDTOs = chatMapper.chatToChatDto(chatService.findAllByGameIdAndFaction(gameId,"global"));
            return ResponseEntity.ok(chatDTOs);
        }*/
        if(gameService.findById(gameId) != null){
            Collection<ChatDTO> chatDTOs = chatMapper.chatToChatDto(chatService.findAllByGameIdAndFaction(gameId,faction));
            return ResponseEntity.ok(chatDTOs);

        }
        throw new GameNotFoundException(gameId);
    }

    @GetMapping(path = "/{gameId}/chat/all")
    public ResponseEntity findAllByGame(@PathVariable("gameId") Integer gameId){
        Collection<ChatDTO> chatDTOS = chatMapper.chatToChatDto(chatService.findAllByGameId(gameId));
        return ResponseEntity.ok(chatDTOS);
    }

    @Operation(summary = "Adds new Message to DB")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="The message is successfully added",
                    content = @Content ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MalformedRequestException.class))})
    })
    @PostMapping("/{gameId}/chat")
    public ResponseEntity add(@PathVariable("gameId") Integer gameId,@RequestBody ChatDTO chatDTO) {
        Chat chat = chatService.add(chatMapper.chatDtoToChat(chatDTO));
        URI location = URI.create("/chat" + chat.getId());
        //Websocket
            //details
            switch (chatDTO.getFaction()){
                case "global":
                    webSocketService.sendMessage("chat/"+gameId,"global");
                    break;
                case "zombie":
                    webSocketService.sendMessage("chat/"+gameId,"zombie");
                    break;
                case "human":
                    webSocketService.sendMessage("chat/"+gameId,"human");
                    break;
                default:
                    throw new MalformedRequestException();
            }

        return ResponseEntity.created(location).build();
    }


    @Operation(summary = "Deletes a specific chat by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = ChatDTO.class))

                    }),
            @ApiResponse(responseCode = "404",
                    description = "No chat associated with the given ID",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = ChatNotFoundException.class))})
    })
    @DeleteMapping("/{gameId}/chat/{chatId}")
    public ResponseEntity delete(@PathVariable("gameId") int gameId, @PathVariable("chatId") int chatId){
        chatRepository.deleteById(chatId);
        return ResponseEntity.noContent().build();
    }

}
