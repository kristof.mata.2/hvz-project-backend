package com.noroff.hvz.controllers;

import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.MalformedRequestException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.mappers.PlayerMapper;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerAdminDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.PlayerDTO;
import com.noroff.hvz.models.DTOs.playerDTOs.CreatePlayerDTO;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.WebSocketService;
import com.noroff.hvz.services.chat.ChatService;
import com.noroff.hvz.services.game.GameService;
import com.noroff.hvz.services.loginUser.LoginUserService;
import com.noroff.hvz.services.player.PlayerService;
import com.noroff.hvz.services.squad.SquadService;
import com.noroff.hvz.services.squadMember.SquadMemberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(exposedHeaders = "Location")
@RequestMapping(path = "api/game")
public class PlayerController {
    private final PlayerService playerService;
    private final PlayerMapper playerMapper;
    private final LoginUserService loginUserService;
    private final GameService gameService;
    private final WebSocketService webSocketService;
    private final SquadMemberService squadMemberService;
    private final ChatService chatService;


    public PlayerController(PlayerService playerService, PlayerMapper playerMapper, LoginUserService loginUserService, GameService gameService, WebSocketService webSocketService, SquadService squadService, SquadMemberService squadMemberService, SquadMemberService squadMemberService1, ChatService chatService) {
        this.playerService = playerService;
        this.playerMapper = playerMapper;
        this.loginUserService = loginUserService;
        this.gameService = gameService;
        this.webSocketService = webSocketService;
        this.squadMemberService = squadMemberService1;
        this.chatService = chatService;
    }

    @Operation(summary = "Retrieves all the players in a game.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = PlayerDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//GET AllByGameId
    @GetMapping("/{gameId}/player")
    public ResponseEntity findAllByGameId(@PathVariable("gameId") Integer gameId) {
        if (gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        Collection<PlayerAdminDTO> playerAdminDTO = playerMapper.playerToPlayerAdminDto(playerService.findAllByGameId(gameId));
        return ResponseEntity.ok(playerAdminDTO);
    }

    @Operation(summary= "Retrieves a specific player by ID in a game.")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = PlayerDTO.class))

                    }),
            @ApiResponse(responseCode = "404",
                    description = "Player does not exist with supplied gameId and playerId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+player",
                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//GET PlayerByGameIdAndPlayerId
    @GetMapping("/{gameId}/player/{playerId}")
    public ResponseEntity<PlayerDTO> findByPlayerId(@PathVariable("gameId") int gameId, @PathVariable("playerId") int playerId) {
        if(gameService.findById(gameId)==null){
            throw new GameNotFoundException(gameId);
        }
        PlayerDTO playerDTO = playerMapper.playerToPlayerDto(playerService.findByGameIdAndPlayerId(gameId, playerId));
        return ResponseEntity.ok(playerDTO);
    }
    //TODO might not be needed, delete if nothing breaks

    /*@Operation(summary= "Retrieves a specific player by loginUserId in a game")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = PlayerDTO.class))

                    }),
            @ApiResponse(responseCode = "404",
                    description = "No player associated with the given ID",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = PlayerNotFoundException.class))})
    })
    @GetMapping("/{gameId}/player/loginuser/{loginUserId}") //http://localhost:8080/api/game/1/player/loginuser/{loginUserId}
    public ResponseEntity getPlayerByLoginUserId(@PathVariable("gameId") int gameId, @PathVariable("loginUserId") int loginUserId){
        PlayerDTO player = playerMapper.playerToPlayerDto(
                playerService.findByLoginUserIdAndGameId(loginUserId, gameId)
        );
        return ResponseEntity.ok(player);
    }*/

    @Operation(summary = "Adds a new Player.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="The player is successfully added",
                    content = @Content ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request.",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MalformedRequestException.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Game does not exist with supplied ID.",
                    content = { @Content(mediaType ="application/json",
                    array = @ArraySchema(schema = @Schema(implementation = GameNotFoundException.class)))})
    })//POST Add
    @PostMapping("{gameId}/player")
    public ResponseEntity add(@PathVariable("gameId") Integer gameId, @RequestBody CreatePlayerDTO createPlayerDTO) {
        Game game = gameService.findById(gameId);
        if(game==null) {
            throw new GameNotFoundException(gameId);
        }
        if(createPlayerDTO.getKeycloakId()!=null){
            createPlayerDTO.setLoginUser(loginUserService.findByKeycloakId(createPlayerDTO.getKeycloakId()).getId());
            createPlayerDTO.setBiteCode(playerService.createRandomBiteCode(4));
        }
        else{
            throw new MalformedRequestException();
        }
        Player player = playerService.add(playerMapper.createPlayerDtoToPlayer(createPlayerDTO));
        game.setHumanCount(game.getHumanCount()+1);
        gameService.update(game);
        URI location = URI.create(gameId+"/player/" + player.getId());
        //Websocket
            //login
            webSocketService.sendMessage("game","create_player");
            //details
            webSocketService.sendMessage("game/"+gameId,"create_player");
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Updates a player")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated the player",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Player does not exist with supplied gameId and playerId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+player",
                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//PUT update
    @PutMapping("/{gameId}/player/{playerId}")
    public ResponseEntity<PlayerDTO> update(@RequestBody PlayerDTO playerDTO, @PathVariable("gameId") int gameId, @PathVariable("playerId") int playerId) {
        if(gameService.findById(gameId)==null) {
            throw new GameNotFoundException(gameId);
        }
        if(playerService.findByGameIdAndPlayerId(gameId,playerId)==null){
            throw new PlayerNotFoundException(playerId);
        }
        playerDTO.setId(playerId);
        playerService.update(
                playerMapper.playerDtoToPlayer(playerDTO)
        );
        //Websocket
            //login
            //details
            //webSocketService.sendMessage("player/"+playerId,"update");
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a specific player by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "204",
                    description = "Player deleted.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = PlayerDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Player does not exist with supplied gameId and playerId.",
                    content = { @Content(mediaType ="application/json",
                    schema = @Schema(implementation = GameNotFoundException.class)),
                            @Content(mediaType ="application/json+player",
                    schema = @Schema(implementation = PlayerNotFoundException.class))})
    })//DELETE delete
    @DeleteMapping(path = "/{gameId}/player/{playerId}")
    public ResponseEntity delete(@PathVariable("gameId") int gameId, @PathVariable("playerId") int playerId) {
        Game game = gameService.findById(gameId);
        if(game==null) {
            throw new GameNotFoundException(gameId);
        }
        Player player = playerService.findByGameIdAndPlayerId(gameId,playerId);
        if(player==null){
            throw new PlayerNotFoundException(playerId);
        }
        if(player.getSquadMember()!=null){
            squadMemberService.deleteById(squadMemberService.findByPlayerId(playerId).getId());
        }
        if(player.isHuman()==true){
            game.setHumanCount(game.getHumanCount()-1);
        }
        chatService.findAllByGameId(gameId).forEach((chat) -> chatService.delete(chat));
        playerService.deleteById(playerId);
        //Websocket
            //login
            webSocketService.sendMessage("game","delete_players");
            //details
            webSocketService.sendMessage("game/"+gameId,"create_player");
        return ResponseEntity.noContent().build();
    }



}
