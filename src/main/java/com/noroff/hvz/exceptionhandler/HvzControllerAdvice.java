package com.noroff.hvz.exceptionhandler;

import com.noroff.hvz.config.HvzErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;

@ControllerAdvice
public class HvzControllerAdvice {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<HvzErrorResponse> handleConstraintViolationException(ConstraintViolationException exception) {
        List<String> errors = exception.getConstraintViolations()
                .stream()
                .map(cv -> cv.getRootBeanClass().getName() + " " + cv.getPropertyPath() + ": " + cv.getMessage())
                .toList();
        HvzErrorResponse errorResponse = new HvzErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage(), errors);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), errorResponse.getStatus());
    }

}
