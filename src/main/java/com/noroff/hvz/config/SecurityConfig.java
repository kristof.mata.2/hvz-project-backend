package com.noroff.hvz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
            .cors().and()
            .sessionManagement().disable()
            .csrf().disable()
            .authorizeHttpRequests( authorize -> authorize
                    .mvcMatchers("/api/game").permitAll()
                    .mvcMatchers("/websocket/**").permitAll()
                    /*.mvcMatchers(HttpMethod.DELETE).hasRole("ADMIN")
                    .mvcMatchers(HttpMethod.PUT).hasRole("ADMIN")
                    .mvcMatchers(HttpMethod.GET, "/api/game/player").hasRole("ADMIN")
                    .mvcMatchers(HttpMethod.GET, "/api/game/kill").hasRole("ADMIN")*/
                    .anyRequest().permitAll()//.authenticated()
            )
            .oauth2ResourceServer()
            .jwt();
            //.jwtAuthenticationConverter(jwtRoleAuthenticationConverter());
        return http.build();
    }
    @Bean
    public JwtAuthenticationConverter jwtRoleAuthenticationConverter(){
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}
