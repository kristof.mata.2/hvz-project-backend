package com.noroff.hvz.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.FIELD;

@Documented
@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GameStatusValidator.class)
public @interface GameStatus {

    String message() default "{game.status.invalid}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
