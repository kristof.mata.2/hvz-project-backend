package com.noroff.hvz.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GameStatusValidator implements ConstraintValidator<GameStatus, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.equals("Registration") || value.equals("In Progress") || value.equals("Complete");
    }

}
