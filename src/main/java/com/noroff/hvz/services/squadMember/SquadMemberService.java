package com.noroff.hvz.services.squadMember;

import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.services.CrudService;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;

@Service
public interface SquadMemberService extends CrudService<SquadMember,Integer> {
    Collection<SquadMember> findAllByName(String name);

    Collection<SquadMember> findAllBySquadId(Integer squadId);

    SquadMember findByPlayerId(int playerId);

    Collection<SquadMember> findAllByGameId(Integer gameId);

    SquadMember findByGameIdAndSquadMemberId(int gameId, int squadMemberId);

}


