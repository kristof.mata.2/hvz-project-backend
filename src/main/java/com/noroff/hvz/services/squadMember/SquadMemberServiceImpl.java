package com.noroff.hvz.services.squadMember;

import com.noroff.hvz.exceptions.SquadMemberNotFoundException;
import com.noroff.hvz.exceptions.SquadNotFoundException;
import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import com.noroff.hvz.repositories.SquadMemberRepository;
import com.noroff.hvz.services.loginUser.LoginUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SquadMemberServiceImpl implements SquadMemberService {

    private final Logger logger = LoggerFactory.getLogger(LoginUserService.class);
    private final SquadMemberRepository squadMemberRepository;

    public SquadMemberServiceImpl(SquadMemberRepository squadMemberRepository){
        this.squadMemberRepository = squadMemberRepository;
    }

    @Override
    public SquadMember findById(Integer integer) {
        return squadMemberRepository.findById(integer).get();
    }

    @Override
    public Collection<SquadMember> findAll() {
        return squadMemberRepository.findAll();
    }

    @Override
    public SquadMember add(SquadMember entity) {
        return squadMemberRepository.save(entity);
    }

    @Override
    public SquadMember update(SquadMember entity) {
        return squadMemberRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        squadMemberRepository.deleteById(id);
    }

    @Override
    public void delete(SquadMember entity) {
        if(squadMemberRepository.existsById(entity.getId())){
            SquadMember squadMember = entity;
            squadMemberRepository.delete(squadMember);
        }else{
            logger.warn("No SquadMember exists with ID: " + entity.getId());
        }
    }

    @Override
    public Collection<SquadMember> findAllByName(String name) {
        return squadMemberRepository.findAllByName(name);
    }

    @Override
    public Collection<SquadMember> findAllBySquadId(Integer squadId) {
        return squadMemberRepository.findAllBySquadId(squadId);
    }

    @Override
    public SquadMember findByPlayerId(int playerId) {
        return squadMemberRepository.findByPlayerId(playerId)
                .orElseThrow(() -> new SquadMemberNotFoundException(playerId));
    }

    @Override
    public Collection<SquadMember> findAllByGameId(Integer gameId) {
        return squadMemberRepository.findAllByGameId(gameId);
    }

    @Override
    public SquadMember findByGameIdAndSquadMemberId(int gameId, int squadMemberId) {
        return squadMemberRepository.findByGameIdAndSquadMemberId(gameId,squadMemberId)
                .orElseThrow(() -> new SquadNotFoundException(gameId,squadMemberId));
    }
}
