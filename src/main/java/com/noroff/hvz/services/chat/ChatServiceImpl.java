package com.noroff.hvz.services.chat;

import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.repositories.ChatRepository;
import com.noroff.hvz.services.chat.ChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ChatServiceImpl implements ChatService {

    private final Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);
    private final ChatRepository chatRepository;
    public ChatServiceImpl(ChatRepository chatRepository){
        this.chatRepository = chatRepository;
    }
    @Override
    public Chat findById(Integer chatId) {
        return chatRepository.findById(chatId).get();
    }

    @Override
    public Collection<Chat> findAll() {
        return chatRepository.findAll();
    }

    @Override
    public Chat add(Chat entity) {
        return chatRepository.save(entity);
    }

    @Override
    public Chat update(Chat entity) {
        return chatRepository.save(entity);
    }

    @Override
    public void deleteById(Integer chatId){
        if(this.chatRepository.existsById(chatId)){
            Chat chat = chatRepository.findById(chatId).get();
            this.chatRepository.delete(chat);
        }else{
            this.logger.warn("No existing chat with Id:" + chatId);
        }
    }

    @Override
    public void delete(Chat entity) {
        this.chatRepository.delete(entity);
    }

    @Override
    public Collection<Chat> findAllByGameIdAndFaction(int gameId, String faction) {
            return chatRepository.findAllByGameIdAndFaction(gameId,faction);
    }

    @Override
    public Collection<Chat> findAllByGameId(int gameId) {
        return chatRepository.findAllByGameId(gameId);
    }


}
