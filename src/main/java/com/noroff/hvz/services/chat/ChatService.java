package com.noroff.hvz.services.chat;

import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.services.CrudService;

import java.util.Collection;

public interface ChatService extends CrudService<Chat, Integer>{
    Collection<Chat> findAllByGameIdAndFaction(int gameId, String faction);
    Collection<Chat> findAllByGameId(int gameId);
}
