package com.noroff.hvz.services.player;

import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.services.CrudService;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public interface PlayerService extends CrudService<Player, Integer> {
    Collection<Player> findAllByName(String name);
    Collection<Player> findAllByGameId(int gameId);
    Player findByGameIdAndPlayerId(int gameId, int playerId);
    Player findByLoginUserIdAndGameId(int loginUserId, int gameId);

    Collection<Player> findByLoginUserId(int loginUserId);

    Collection<Player> findByKeycloakId(String keycloakId);

    String createRandomBiteCode(int length);

    Player findByGameIdBiteCode(Integer gameId, String biteCode);
}
