package com.noroff.hvz.services.player;

import com.noroff.hvz.exceptions.NoPlayerWithBiteCodeException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.models.Player;
import com.noroff.hvz.repositories.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService{

    public static final String INVALID_BITE_CODE_LENGTH_MESSAGE = "Invalid bite code length: ";

    private final PlayerRepository playerRepository;
    private final Logger logger = LoggerFactory.getLogger(PlayerServiceImpl.class);

    public PlayerServiceImpl(PlayerRepository pLayerRepository) {
        this.playerRepository = pLayerRepository;
    }

    @Override
    public Player findById(Integer integer) {
        return playerRepository.findById(integer).get();
    }

    @Override
    public Collection<Player> findAll() {
        return playerRepository.findAll();
    }

    @Override
    public Player add(Player entity) {
        return playerRepository.save(entity);
    }

    @Override
    public Player update(Player entity) {
        return playerRepository.save(entity);
    }

    @Override
    public void deleteById(Integer playerId) {
        Player player = playerRepository.findById(playerId).get();
        player.setSquadMember(null);
        playerRepository.delete(player);
    }

    @Override
    public void delete(Player entity) {
        if(playerRepository.existsById(entity.getId())){
            Player player = entity;
            playerRepository.delete(player);
        }
        else {
            logger.warn("There is no player with Id:" + entity.getId());
        }

    }

    @Override
    public Collection<Player> findAllByName(String name) {
        return playerRepository.findAllByName(name);
    }

    @Override
    public Collection<Player> findAllByGameId(int gameId) {
        return playerRepository.findAllByGameId(gameId);
    }

    @Override
    public Player findByGameIdAndPlayerId(int gameId, int playerId) {
        return playerRepository.findByGameIdAndPlayerId(gameId, playerId)
                .orElseThrow(() -> new PlayerNotFoundException(gameId, playerId));
    }

    @Override
    public Player findByLoginUserIdAndGameId(int loginUserId, int gameId) {
        return playerRepository.findPlayerByLoginUserIdAndGameId(loginUserId, gameId)
                .orElseThrow(() -> new PlayerNotFoundException(gameId, loginUserId));
    }

    @Override
    public Collection<Player> findByLoginUserId(int loginUserId) {
        return playerRepository.findAllByLoginUserId(loginUserId);
    }

    @Override
    public Collection<Player> findByKeycloakId(String keycloakId) {
        return playerRepository.findAllByKeycloakId(keycloakId);
    }

    public String createRandomBiteCode(int length) {
        if (length <= 0 || length > 1000) {
            throw new IllegalArgumentException(INVALID_BITE_CODE_LENGTH_MESSAGE + length);
        }

        StringBuilder name;
        do {
            name = new StringBuilder();
            for (int i = 0; i < length; i++) {
                int v = 1 + (int) (Math.random() * 26);
                char c = (char) (v + 'a' - 1);
                name.append(c);
            }
        }
        while(playerRepository.findBiteCode(name.toString())!=null);

        return name.toString();
    }

    @Override
    public Player findByGameIdBiteCode(Integer gameId, String biteCode) {
        return playerRepository.findByGameIdBiteCode(gameId, biteCode)
                .orElseThrow(() -> new NoPlayerWithBiteCodeException(biteCode));
    }


}
