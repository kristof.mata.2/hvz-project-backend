package com.noroff.hvz.services.squad;

import com.noroff.hvz.models.Squad;
import com.noroff.hvz.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface SquadService extends CrudService<Squad,Integer> {
    Collection<Squad> findAllByName(String name);

    Collection<Squad> findAllByGameId(Integer id);

    Squad findBySquadIdAndGameId(Integer squadId, Integer gameId);

    Squad findByNameInGame(Integer gameId, String squadName);

}


