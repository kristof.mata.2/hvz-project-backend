package com.noroff.hvz.services.squad;

import com.noroff.hvz.exceptions.SquadNotFoundException;
import com.noroff.hvz.models.LoginUser;
import com.noroff.hvz.models.Squad;
import com.noroff.hvz.repositories.LoginUserRepository;
import com.noroff.hvz.repositories.SquadRepository;
import com.noroff.hvz.services.loginUser.LoginUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SquadServiceImpl  implements SquadService{

    private final Logger logger = LoggerFactory.getLogger(LoginUserService.class);
    private final SquadRepository squadRepository;

    public SquadServiceImpl(SquadRepository squadRepository){
        this.squadRepository = squadRepository;
    }

    @Override
    public Squad findById(Integer integer) {
        return squadRepository.findById(integer).get();
    }

    @Override
    public Collection<Squad> findAll() {
        return squadRepository.findAll();
    }

    @Override
    public Squad add(Squad entity) {
        return squadRepository.save(entity);
    }

    @Override
    public Squad update(Squad entity) {
        return squadRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        squadRepository.deleteById(id);
    }

    @Override
    public void delete(Squad entity) {
        if(squadRepository.existsById(entity.getId())){
            Squad squad = entity;
            squadRepository.delete(squad);
        }else{
            logger.warn("No Squad exists with ID: " + entity.getId());
        }
    }

    @Override
    public Collection<Squad> findAllByName(String name) {
        return squadRepository.findAllByName(name);
    }

    @Override
    public Collection<Squad> findAllByGameId(Integer gameId) {
        return squadRepository.findAllByGameId(gameId);
    }

    @Override
    public Squad findBySquadIdAndGameId(Integer squadId, Integer gameId) {
        //Squad result = squadRepository.findSquadByIdAndGameId(gameId, squadId);
        return squadRepository.findBySquadIdAndGameId(gameId, squadId)
                .orElseThrow(() -> new SquadNotFoundException(gameId, squadId));
    }

    @Override
    public Squad findByNameInGame(Integer gameId, String squadName) {
        return squadRepository.findByNameAndGameId(gameId, squadName)
                .orElseThrow(() -> new SquadNotFoundException(gameId, squadName));
    }
}
