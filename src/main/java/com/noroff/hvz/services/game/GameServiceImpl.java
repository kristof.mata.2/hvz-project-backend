package com.noroff.hvz.services.game;

import com.noroff.hvz.exceptions.GameNotFoundException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.models.Game;
import com.noroff.hvz.repositories.GameRepository;
import com.noroff.hvz.services.WebSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GameServiceImpl implements GameService {

    private final Logger logger = LoggerFactory.getLogger(GameServiceImpl.class);
    private final GameRepository gameRepository;



    public GameServiceImpl(GameRepository gameRepository){
        this.gameRepository = gameRepository;
    }

    @Override
    public Game findById(Integer gameId) {
        return gameRepository.findById(gameId)
                .orElseThrow(() -> new GameNotFoundException(gameId));
    }

    @Override
    public Collection<Game> findAll() {
        return gameRepository.findAllGame();
    }

    @Override
    public Game add(Game entity) {
        return gameRepository.save(entity);
    }

    @Override
    public Game update(Game entity) {
        return gameRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(this.gameRepository.existsById(id)){
            Game game = gameRepository.findById(id).get();
            this.gameRepository.delete(game);
        }else{
            this.logger.warn("No Game exists with ID: " + id);
        }

    }

    @Override
    public void delete(Game entity) {
        this.gameRepository.delete(entity);
    }
}
