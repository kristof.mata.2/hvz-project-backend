package com.noroff.hvz.services.kill;
import com.noroff.hvz.models.DTOs.killDTOs.CreateKillDTO;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface KillService extends CrudService<Kill,Integer> {
    Collection<Kill> findAllByGameId(int gameId);
    Kill findByGameIdAndKillId(int gameId, int killId);
}
