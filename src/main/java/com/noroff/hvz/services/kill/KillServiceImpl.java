package com.noroff.hvz.services.kill;
import com.noroff.hvz.exceptions.KillNotFoundException;
import com.noroff.hvz.models.Kill;
import com.noroff.hvz.repositories.KillRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;

@Service
@Transactional
public class KillServiceImpl implements KillService{

    private final KillRepository killRepository;

    public KillServiceImpl(KillRepository killRepository){

        this.killRepository = killRepository;
    }

    @Override
    public Kill findById(Integer killId) {
        return killRepository.findById(killId).
                orElseThrow(() -> new KillNotFoundException(killId));
    }

    @Override
    public Collection<Kill> findAll(){
        return killRepository.findAll();
    }

    @Override
    public Kill add(Kill entity) {
        return killRepository.save(entity);
    }

    @Override
    public Kill update(Kill entity) {
        return killRepository.save(entity);
    }

    @Override
    public void deleteById(Integer killId) {
        if(killRepository.existsById(killId)) {
            killRepository.deleteById(killId);
        } else
            throw new KillNotFoundException(killId);
    }

    public void delete(Kill entity) {
        if(killRepository.existsById(entity.getId())){
            Kill kill = entity;
            killRepository.delete(kill);
        } else
            throw new KillNotFoundException(entity.getId());
    }
    @Override
    public Collection<Kill> findAllByGameId(int gameId) {
        return killRepository.findAllByGameId(gameId);
    }
    @Override
    public Kill findByGameIdAndKillId(int gameId, int killId) {
        return killRepository.findByGameIdAndKillId(gameId, killId)
                .orElseThrow(() -> new KillNotFoundException(gameId, killId));
    }
}
