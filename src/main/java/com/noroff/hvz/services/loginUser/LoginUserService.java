package com.noroff.hvz.services.loginUser;

import com.noroff.hvz.models.LoginUser;
import com.noroff.hvz.services.CrudService;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.Optional;

@Service
public interface LoginUserService extends CrudService<LoginUser,Integer> {
    //additional business logic
    Collection<LoginUser> findAllByName(String name);

    LoginUser findByKeycloakId(String keycloakId);
}