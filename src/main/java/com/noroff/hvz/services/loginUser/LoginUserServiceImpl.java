package com.noroff.hvz.services.loginUser;

import com.noroff.hvz.exceptions.LoginUserNotFoundException;
import com.noroff.hvz.exceptions.PlayerNotFoundException;
import com.noroff.hvz.models.LoginUser;
import com.noroff.hvz.repositories.LoginUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.Optional;

@Service
public class LoginUserServiceImpl implements LoginUserService{

    private final Logger logger = LoggerFactory.getLogger(LoginUserService.class);
    private final LoginUserRepository loginUserRepository;

    public LoginUserServiceImpl(LoginUserRepository loginUserRepository){
        this.loginUserRepository = loginUserRepository;
    }

    @Override
    public LoginUser findById(Integer integer) {
        return loginUserRepository.findById(integer).get();
    }

    @Override
    public Collection<LoginUser> findAll() {
        return loginUserRepository.findAll();
    }

    @Override
    public LoginUser add(LoginUser entity) {
        return loginUserRepository.save(entity);
    }

    @Override
    public LoginUser update(LoginUser entity) {
        return loginUserRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        loginUserRepository.deleteById(id);
    }

    @Override
    public void delete(LoginUser entity) {
        if(loginUserRepository.existsById(entity.getId())){
            LoginUser loginUser = entity;
            loginUserRepository.delete(loginUser);
        }else{
            logger.warn("No LoginUser exists with ID: " + entity.getId());
        }
    }

    @Override
    public Collection<LoginUser> findAllByName(String name) {
        return loginUserRepository.findAllByName(name);
    }

    @Override
    public LoginUser findByKeycloakId(String keycloakId) {
        try{
            return loginUserRepository.findByKeycloakId(keycloakId)
                    .orElseThrow(() -> new LoginUserNotFoundException(keycloakId));
        }
        catch(Exception e)
        {
            return null;
        }
    }
}
