package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Kill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface KillRepository extends JpaRepository <Kill, Integer> {
    @Query(nativeQuery = true, value="SELECT * FROM tb_kill k WHERE k.game_id= :gameId ORDER BY k.id")
    Collection<Kill> findAllByGameId(@Param("gameId") int gameId);
    @Query(nativeQuery = true, value="SELECT * FROM tb_kill k WHERE k.game_id= :gameId and k.id= :killId")
    Optional<Kill> findByGameIdAndKillId(@Param("gameId") int gameId, @Param("killId") int killId);
}
