package com.noroff.hvz.repositories;

import java.util.Optional;
import java.util.Set;

import com.noroff.hvz.models.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginUserRepository extends JpaRepository<LoginUser,Integer> {
    @Query("select s from LoginUser s where s.firstName like %?1%")
    Set<LoginUser> findAllByName(String name);
    @Query(nativeQuery = true, value="SELECT * FROM tb_user u WHERE u.keycloak_id= :keycloakId")
    Optional<LoginUser> findByKeycloakId(@Param("keycloakId") String keycloakId);
}