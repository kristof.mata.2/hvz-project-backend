package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface GameRepository extends JpaRepository<Game,Integer> {
    @Query(nativeQuery = true, value="SELECT * FROM tb_game g ORDER BY g.id")
    Collection<Game> findAllGame();
}
