package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Game;
import com.noroff.hvz.models.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer> {
    @Query("select p from Player p")
    Collection<Player> findAllByName(String name);

    Collection<Player> findAllByGameId(int gameId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_player p WHERE p.game_id= :gameId and p.id= :playerId")
    Optional<Player> findByGameIdAndPlayerId(@Param("gameId") int gameId, @Param("playerId") int playerId);

    //Using nativeQuery to search by login_user_id
    @Query(nativeQuery = true, value="SELECT * FROM tb_player p WHERE p.game_id= :gameId and p.login_user_id= :loginUserId")
    Optional<Player> findPlayerByLoginUserIdAndGameId(@Param("gameId") int gameId, @Param("loginUserId") int loginUserId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_player p WHERE p.login_user_id= :loginUserId")
    Collection<Player> findAllByLoginUserId(@Param("loginUserId") int loginUserId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_player INNER JOIN tb_user ON tb_user.id = tb_player.login_user_id WHERE tb_user.keycloak_id= :keycloakId")
    Collection<Player> findAllByKeycloakId(@Param("keycloakId") String keycloakId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_player p WHERE p.game_id= :gameId and p.bite_code= :biteCode")
    Optional<Player> findByGameIdBiteCode(@Param("gameId") int gameId, @Param("biteCode") String biteCode);

    @Query(nativeQuery = true, value="SELECT * FROM tb_player p WHERE p.bite_code= :biteCode")
    Player findBiteCode(@Param("biteCode") String biteCode);
}
