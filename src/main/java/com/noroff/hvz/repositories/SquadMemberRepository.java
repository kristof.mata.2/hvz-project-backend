package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Squad;
import com.noroff.hvz.models.SquadMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface SquadMemberRepository extends JpaRepository<SquadMember,Integer> {
    @Query(nativeQuery = true, value="SELECT * FROM tb_squadmember s WHERE s.squad_id= :squadId")
    Collection<SquadMember> findAllBySquadId(@Param("squadId") int squadId);

    @Query("select s from SquadMember s where s.rank like %?1%")
    Collection<SquadMember> findAllByName(String name);

    @Query(nativeQuery = true, value="SELECT * FROM tb_squadMember s WHERE s.player_id= :playerId")
    Optional<SquadMember> findByPlayerId(@Param("playerId") int playerId);
    @Query(nativeQuery = true, value="SELECT * FROM tb_squadmember INNER JOIN tb_squad ON tb_squad.id = tb_squadmember.squad_id WHERE tb_squad.game_id= :gameId")
    Collection<SquadMember> findAllByGameId(@Param("gameId") int gameId);
    @Query(nativeQuery = true, value="SELECT * FROM tb_squadmember INNER JOIN tb_squad ON tb_squad.id = tb_squadmember.squad_id " +
            "WHERE tb_squad.game_id= :gameId and tb_squadmember.id= :squadMemberId")
    Optional<SquadMember> findByGameIdAndSquadMemberId(@Param("gameId") int gameId, @Param("squadMemberId") int squadMemberId);
}
