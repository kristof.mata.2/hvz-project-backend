package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Chat;
import com.noroff.hvz.models.Kill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {
    @Query(nativeQuery = true, value="SELECT * FROM tb_chat k WHERE k.game_id= :gameId AND k.faction LIKE :chatFaction")
    Collection<Chat> findAllByGameIdAndFaction(@Param("gameId") int gameId, String chatFaction);

    @Query(nativeQuery = true, value = "SELECT * FROM tb_chat k WHERE k.game_id = :gameId")
    Collection<Chat> findAllByGameId(@Param("gameId") int gameId);
}
