package com.noroff.hvz.repositories;

import com.noroff.hvz.models.Squad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface SquadRepository extends JpaRepository<Squad,Integer> {
    @Query("select s from Squad s where s.name like %?1%")
    Set<Squad> findAllByName(String name);
    @Query(nativeQuery = true, value="SELECT * FROM tb_squad s WHERE s.game_id= :gameId")
    Set<Squad> findAllByGameId(@Param("gameId") int gameId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_squad s WHERE s.game_id= :gameId and s.id= :squadId")
    Optional<Squad> findBySquadIdAndGameId(@Param("gameId") int gameId, @Param("squadId") Integer squadId);

    @Query(nativeQuery = true, value="SELECT * FROM tb_squad s WHERE s.game_id= :gameId and s.name= :squadName")
    Optional<Squad> findByNameAndGameId(@Param("gameId") int gameId, @Param("squadName") String squadName);

}
