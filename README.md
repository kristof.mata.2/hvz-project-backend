# Humans Vs Zombies API

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Web API and database** with **Spring**.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This is a web API contains Users, Games, Players, Squads, Squadmembers, Kills and Chats as entities for the Humans Vs Zombies game
. The main entity relations are as follows:

- *one* **user** can belong to *many* **players**
- *one* **game** can contain *many* **squads**
- *one* **game** can contain *many* **kills**
- *one* **game** can contain *many* **players**
- *one* **game** can contain *many* **chats**
- *one* **player** can belong to *one* **squadmember**
- *one* **player** can contain *many* **chats**
- *one* **game** can contain to *many* **players**
- *one* **game** can contain to *many* **squads**
- *one* **game** can contain to *many* **chats**
- *one* **game** can belong to *many* **kills**
- *many* **kills** can appear in *one* **game**
- *many* **chats** can belong to *one* **player**
- *many* **chats** can appear in *one* **game**

## API endpoints

- GET api/game/loginuser – get all users
- POST api/game – adds a new user
- GET api/game – get all games
- GET api/game/:gameId – get game by ID
- POST api/game – adds a new game
- PUT api/game/:gameId  – updates a game
- DELETE api/game/:gameId  – deletes a game
- GET api/game/:gameId/player – get all players
- GET api/game/:gameId/player/:playerId – gets player by ID
- POST api/game/:gameId/player – add new player
- PUT api/game/:gameId/player/:playerId  – updates player
- DELETE api/game/:gameId/player/:playerId  – deletes player
- GET api/game/:gameId/chat – get all messages
- POST api/game/:gameId/chat – add new message
- DELETE api/game/:gameId/chat/:chatId  – deletes message
- GET api/game/:gameId/kill – get all kills
- GET api/game/:gameId/kill/:killId – gets kill by ID
- POST api/game/:gameId/kill – add new kill
- PUT api/game/:gameId/kill/:killId  – updates kill
- DELETE api/game/:gameId/kill/:killId  – deletes kill
- GET api/game/:gameId/squad – get all squads
- GET api/game/:gameId/squad/:squadId – gets squad by ID
- POST api/game/:gameId/player – add new squad
- DELETE api/game/:gameId/squad/:squadId  – deletes squad
- GET api/game/:gameId/squadmember – get all squadmembers
- GET api/game/:gameId/squadmember/:squadmemberId – gets squadmember by ID
- POST api/game/:gameId/squadmember – add new squadmember
- PUT api/game/:gameId/squadmember/:squadmemberId  – updates squadmember
- DELETE api/game/:gameId/squadmember/:squadmemberId  – deletes squadmember


## Local running

1. Install:

- Java SDK - https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
- VS Code - https://code.visualstudio.com/
- Node.js and NPM - https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
- PostgreSQL - https://www.postgresql.org/download/

2. Clone the project repositories

- BE - https://gitlab.com/kristof.mata.2/hvz-project-backend.git
- FE - https://gitlab.com/kristof.mata.2/hvz-project-frontend.git

3. Create a postgres database named **hvzdb** and set password **postgres**

4. Install dependencies

- BE - IntelliJ - run **Gradle** to install dependencies
- FE - VS Code - run **npm install** from terminal

5. Run the application

- Backend by the main method from **src/main/java/com/noroff/hvz/HvzApplication.java**
- Frontend by **ng serve** from terminal

6. Local pages

- BE - http://localhost:5000/
- FE - http://localhost:4200/


## Usage

1. Test the application with one of our test users: 
   - as a regular **user** with username: ```user``` password: ```user```
   - as an **admin** with username:```admin``` password: ```admin```
3. You can also register your own user through the app.

## Maintainers
- [Kristof Mata](https://gitlab.com/kristof.mata.2)
- [Bela Toth](https://gitlab.com/tothbt)
- [Adam Olah](https://gitlab.com/adam-olah93)
- [Krisztina Pelei](https://gitlab.com/kokriszti)
- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
